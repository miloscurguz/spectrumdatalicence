﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Admin.SpectrumData.Clients
{
    public class ApiClient
    {
        private readonly Func<IRestClient> _client;
        private readonly Func<IRestRequest> _request;

        public ApiClient(Func<IRestClient> client, Func<IRestRequest> request)
        {
            _client = client;
            _request = request;
        }

        protected IRestClient CreateClient()
        {
            _client().BaseUrl = new Uri(ConfigurationManager.AppSettings["Api:ApiUrl"]);
            return _client();
        }

        protected IRestRequest CreateGetRequest(string resource)
        {
            return CreateRequest(resource);
        }

        protected IRestRequest CreatePostRequest(string resource)
        {
            return CreateRequest(resource, Method.POST);
        }

        protected IRestRequest CreatePutRequest(string resource)
        {
            return CreateRequest(resource, Method.PUT);
        }

        private IRestRequest CreateRequest(string resource, Method method = Method.GET)
        {
            var request = _request();
            request.Resource = resource;
            request.Method = method;
            request.RequestFormat = DataFormat.Json;
            return request;
        }

   
    }
}