﻿
using Admin.SpectrumData.Clients;
using Admin.SpectrumData.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Admin.SpectrumData.Services
{
    public class ApiService : ApiClient, IApiService
    {
        public ApiService(Func<IRestClient> client, Func<IRestRequest> request) : base(client, request)
        {

        }

        public async Task<List<Licenca>> VratiSveLicence()
        {
             var request = CreateGetRequest("api/sve_licence");
                var response = await CreateClient().GetAsync<List<Licenca>>(request);
                var result = response;
                return result;
            
        }
    }
}