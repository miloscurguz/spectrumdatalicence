﻿
using Admin.SpectrumData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.SpectrumData.Services
{
    public interface IApiService
    {
        Task<List<Licenca>> VratiSveLicence();
    }
}
