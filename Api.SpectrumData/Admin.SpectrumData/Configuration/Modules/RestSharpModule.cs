﻿using Autofac;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Module = Autofac.Module;
namespace Admin.SpectrumData.Configuration.Modules
{
    public class RestSharpModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RestClient>().As<IRestClient>().InstancePerLifetimeScope();
            builder.RegisterType<RestRequest>().As<IRestRequest>().InstancePerLifetimeScope();
        }
    }
}