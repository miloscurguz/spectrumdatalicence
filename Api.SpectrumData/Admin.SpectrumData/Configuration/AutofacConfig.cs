﻿using Admin.SpectrumData.Configuration.Modules;
using Admin.SpectrumData.Services;
using Autofac;
using Autofac.Integration.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Admin.SpectrumData.Configuration
{
    public class AutofacConfig
    {
        public static void Register(string assemblyName)
        {
            var containerBuilder = new ContainerBuilder();
            containerBuilder.RegisterType<ApiService>().As<IApiService>();

            containerBuilder.RegisterModule(new RestSharpModule());
            containerBuilder.RegisterControllers(Assembly.GetExecutingAssembly());
            IContainer container = containerBuilder.Build();
         
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}