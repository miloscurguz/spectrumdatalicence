﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Admin.SpectrumData.Models
{
    public class Licenca
    {
        public int Id { get; set; }
        public string Uredjaj_Id { get; set; }
        public string GUID { get; set; }
        public int Aplikacija_Id { get; set; }
        public int Licenca_Id { get; set; }
        public Nullable<int> Kompanija_Id { get; set; }
        public string Verzija { get; set; }
        public DateTime DatumStart { get; set; }
        public DateTime DatumKraj { get; set; }
        public string Napomena { get; set; }
        public bool Aktivna { get; set; }
    }
}