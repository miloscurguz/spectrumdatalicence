//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Api.SpectrumData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Licence
    {
        public int Id { get; set; }
        public string Uredjaj_Id { get; set; }
        public string GUID { get; set; }
        public int Aplikacija_Id { get; set; }
        public int Licenca_Id { get; set; }
        public Nullable<int> Kompanija_Id { get; set; }
        public string Verzija { get; set; }
        public Nullable<System.DateTime> DatumStart { get; set; }
        public Nullable<System.DateTime> DatumKraj { get; set; }
        public string Napomena { get; set; }
        public bool Aktivna { get; set; }

        public virtual Aplikacije Aplikacije { get; set; }
        public virtual Kompanje Kompanje { get; set; }
        public virtual TipLicence TipLicence { get; set; }
    }
}
