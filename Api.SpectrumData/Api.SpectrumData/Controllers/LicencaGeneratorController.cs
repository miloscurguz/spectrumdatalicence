﻿using Api.SpectrumData.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Web.Http;

namespace Api.SpectrumData.Controllers
{
    public class LicencaGeneratorController : ApiController
    {


        e001847Entities1 db = new e001847Entities1();
       
        // GET api/<controller>
        [Route("api/probna_licenca")]
        [HttpGet]
        public Licence KreirajProbnuLicencu(string Uredjaj_Id, string GUID, int Aplikacija_Id, string Verzija)
        {
            var licenca = new Licence()
            {
                Uredjaj_Id = Uredjaj_Id,
                GUID = GUID,
                Aplikacija_Id = Aplikacija_Id,
                Verzija = Verzija,
                Licenca_Id = (int)TipLicenceEN.Probna,
                Kompanija_Id = null,
                DatumStart = DateTime.Now,
                DatumKraj = DateTime.Now.AddDays(3),
                Napomena = "Probna Licenca na dan " + DateTime.Now.ToShortDateString(),
                Aktivna = true
            };
            try
            {
                db.Licences.Add(licenca);
                db.SaveChanges();
                return licenca;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("api/promeni_licencu")]
        [HttpPost]
        public Licence PromeniLicencu(Licence licenca)
        {

            try
            {
                var licencaFromDb = db.Licences.Where(x => x.Uredjaj_Id == licenca.Uredjaj_Id && x.GUID == licenca.GUID).SingleOrDefault();

                licencaFromDb.Aplikacija_Id = licenca.Aplikacija_Id;
                licencaFromDb.Verzija = licenca.Verzija;
                licencaFromDb.Licenca_Id = licenca.Licenca_Id;
                licencaFromDb.Kompanija_Id = licenca.Kompanija_Id;
                licencaFromDb.DatumStart = licenca.DatumStart;
                licencaFromDb.DatumKraj = licenca.DatumKraj;
                licencaFromDb.Napomena = licenca.Napomena;
                licencaFromDb.Aktivna = licenca.Aktivna;

                db.SaveChanges();
                return licencaFromDb;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [Route("api/proveri_licencu")]
        [HttpGet]
        public Licence ProveriLicencu(string Uredjaj_Id, string GUID)
        {
            try
            {
                return db.Licences.Where(x => x.Uredjaj_Id == Uredjaj_Id && x.GUID == GUID).SingleOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        [Route("api/sve_licence")]
        //[KnownType(typeof(Kompanje))]
        [HttpGet]
        public List<Licence> VratiSveLicence()
        {
            db.Configuration.ProxyCreationEnabled = false;
            try
            {
                var licence= db.Licences.ToList();
                return licence;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

    }
}