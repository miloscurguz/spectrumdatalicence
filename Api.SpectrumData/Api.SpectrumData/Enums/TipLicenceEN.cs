﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.SpectrumData.Enums
{
    public enum TipLicenceEN
    {
        Probna=1,
        Vremenska=2,
        Neogranicena=3
    }
}